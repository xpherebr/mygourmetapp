import React from 'react';
import styled from 'styled-components/native';

const Container =  styled.SafeAreaView`
    flex: 1;
    margin-top: 10px;
`;
const List = styled.FlatList``;
const ListView = styled.TouchableOpacity`
    flex-direction: row;
    background-color:#fff;
    padding-top:10px;
    border-radius:10px;
`;
const ListViewImg = styled.Image``;
const ListViewContent = styled.View``;
const ListViewContentDescription = styled.Text`
    font-size:14px;
    color: #000;
    font-weight: bold;
`;
const ListViewContentChief = styled.Text`
    font-size:12px;
    color: #6b6b6b;
`;
const ListViewContentTime = styled.Text`
    font-size:12px;
    color: #6b6b6b;
`;
const ListViewContentStar = styled.View`
    flex: 1;
    flex-direction:row;
`;
const ListViewContentStarImg = styled.Image`
    margin-top: 3px;
    margin-right:5px;
`;
const ListViewContentStarTxt = styled.Text`
    font-size:12px;
    color: #6b6b6b;
`;
const ListViewValuation = styled.View`
    flex: 1;
    margin-right: 5px;
`;
const ListViewValuationView = styled.View`
    flex: 1;
    flex-direction:row;
    margin-right: 5px;
    align-items:flex-start;
    justify-content:flex-end;
`;
const ListViewValuationViewText = styled.Text`
    font-size:12px;
    color: #6b6b6b;
`;
const ListViewValuationViewImg = styled.Image`
    margin:3px;
`;
const ListViewFavorite = styled.View`
    flex: 1;
    flex-direction:row;
    margin-right: 5px;
    align-items:flex-start;
    justify-content:flex-end;
`;
const ListViewFavoriteBtn = styled.TouchableOpacity`
`;
const ListViewFavoriteImg = styled.Image`
    margin:3px;
`;

export default ({data}) => {
    const handlerChoice = (key) =>{
        alert(key);
    }
    return(
        <Container>
            <List 
                data={data} 
                vartical
                showsVerticalScrollIndicator={false}
                keyExtractor={(item) => String(item.id)} 
                snapToAlignment={'Start'}
                scrollEventthrottle={16}
                decelerationRate="fast"
                renderItem={({item}) => 
                    <ListView activeOpacity={1} onPress={()=>handlerChoice(item.id)}>
                        <ListViewImg style={{
                            height: 80,
                            width: 80,
                            marginHorizontal:10,
                            borderRadius:10
                        }} source={{uri:item.uri}} resizeMode='contain'/>
                        <ListViewContent>
                            <ListViewContentDescription>{item.description}</ListViewContentDescription>
                            <ListViewContentChief>Chefe: {item.chief}</ListViewContentChief>
                            <ListViewContentTime>Tempo de Preparo: {item.timeforpreppare}</ListViewContentTime>
                            <ListViewContentStar>
                                <ListViewContentStarImg style={{
                                    height: 10,
                                    width: 10,
                                }} source={require('../assets/star-full.png')} resizeMode='contain' />
                                <ListViewContentStarTxt>{item.stars}</ListViewContentStarTxt>
                            </ListViewContentStar>
                        </ListViewContent>
                        <ListViewValuation>
                            <ListViewValuationView>
                                <ListViewValuationViewText>{item.views}</ListViewValuationViewText>
                                <ListViewValuationViewImg style={{
                                    height: 10,
                                    width: 10,
                                }} source={require('../assets/view.png')} resizeMode='contain' />
                            </ListViewValuationView>
                            <ListViewFavorite>
                                <ListViewFavoriteBtn>
                                <ListViewFavoriteImg style={{
                                        height: 20,
                                        width: 20,
                                    }} 
                                    source={ item.favorite ?require('../assets/coracao-full.png'):require('../assets/coracao.png')} resizeMode='contain' />
                                </ListViewFavoriteBtn>
                            </ListViewFavorite>
                        </ListViewValuation>
                    </ListView>
                }
            />
        </Container>
    );
}