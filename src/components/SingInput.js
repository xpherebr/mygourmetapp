import React from 'react';
import styled from 'styled-components/native';

const InputArea =styled.View`
    width: 100%;
    height: 60px;
    background-Color:#FFDEAD;
    flex-direction:row;
    border-radius: 30px;
    padding-left: 15px;
    align-items:center;
    margin-bottom: 15px;
`;
const IconImg = styled.Image`
    width: 200px;
    height: 200px;
`;
const Input = styled.TextInput`
    flex:1;
    font-size: 16px;
    color:#D2691E;
    margin-left:10px;
`;

export default ({placeholder,value,onChangeText,password}) => {
    return(
        <InputArea>
            <Input
                placeholder={placeholder} 
                placeholderTextColor="#F4A460"
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={password}
            />
        </InputArea>
    );
}