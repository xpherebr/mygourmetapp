import React from 'react';
import styled from 'styled-components/native';

const FeedBox = styled.View`
    margin-top:3px;
    margin-bottom:3px;
    background-color:#FFF;
`;
const FeedHeader = styled.View`
    flex: 1;
    flex-direction:row;
    justify-content:space-between;
    align-items:center;
`;
const FeedHeaderContent = styled.View`
    flex: 1;
    flex-direction:row;
    height:60px;
    margin-top:10px;
    margin-left:10px;
`;
const FeedHeaderContentText = styled.View`
    flex: 1;
    height:60px;
`;
const FeedHeaderAvatar = styled.Image`
    height: 50px;
    width: 50px;
    border-radius:25px;
`;
const FeedHeaderTitle = styled.Text`
    font-size:16px;
    font-weight:bold;
    color:#444444;
    margin-left:10px;
    margin-top:-5px;
`;
const FeedHeaderText = styled.Text`
    font-size:12px;
    color:#6b6b6b;
    margin-left:10px;
    margin-top:-3px;
`;
const FeedHeaderFavorite = styled.View`
    flex: 1;
    flex-direction:row;
    margin-right: 5px;
    align-items:flex-start;
    justify-content:flex-end;
`;
const FeedHeaderFavoriteBtn = styled.TouchableOpacity`
`;
const FeedHeaderFavoriteImg = styled.Image`
    margin:3px;
`;
export const FeedBody = styled.View`
    flex: 1;
    height:350px;
`;
export const FeedBodyImg = styled.Image`
    width: 100%;
    height: 100%;
`;
export const FeedFooter = styled.View`
    flex: 1;
    flex-direction:row;
    height:45px;
`;
const FeedFooterItem = styled.TouchableOpacity`
    flex:1;
    align-items:center;
    justify-content:center;
    margin-top: 5px;
`;
const FeedFooterImg = styled.Image`
    height:15px;
    width:15px;
`;
const FeedFooterText = styled.Text`
    color:#444444;
    font-size: 10px;
`;

export default ({data}) => {
    return(
        <FeedBox>
           <FeedHeader>
               <FeedHeaderContent>
                    <FeedHeaderAvatar source={{uri:data.avatar}}  />
                    <FeedHeaderContentText>
                        <FeedHeaderTitle>{data.name}</FeedHeaderTitle>
                        <FeedHeaderText>{data.status}</FeedHeaderText>
                        <FeedHeaderText>{data.issuedate}</FeedHeaderText>
                    </FeedHeaderContentText>
               </FeedHeaderContent>
               <FeedHeaderFavorite>
                <FeedHeaderFavoriteBtn>
                    <FeedHeaderFavoriteImg style={{
                        height: 20,
                        width: 20,
                    }} 
                        source={ data.favorite ?require('../assets/likeactive.png'):require('../assets/like.png')} resizeMode='contain' />
                    </FeedHeaderFavoriteBtn>
                </FeedHeaderFavorite>
           </FeedHeader>
           <FeedBody>
                <FeedBodyImg source={{uri:data.uri}} resizeMode='cover' />
           </FeedBody>
           <FeedFooter>
                <FeedFooterItem onPress={()=>{}}>
                    <FeedFooterImg source={require('../assets/favorito.png')} resizeMode='contain'/>
                    <FeedFooterText>Favoritar</FeedFooterText>
                </FeedFooterItem>
                <FeedFooterItem onPress={()=>{}}>
                    <FeedFooterImg source={require('../assets/enviar.png')} resizeMode='contain'/>
                    <FeedFooterText >Privado</FeedFooterText>
                </FeedFooterItem>
                <FeedFooterItem onPress={()=>{}}>
                    <FeedFooterImg source={require('../assets/comentar.png')} resizeMode='contain'/>
                    <FeedFooterText>Comentar</FeedFooterText>
                </FeedFooterItem>
                <FeedFooterItem onPress={()=>{}}>
                    <FeedFooterImg source={require('../assets/compartilhar.png')} resizeMode='contain'/>
                    <FeedFooterText>Compartilhar</FeedFooterText>
                </FeedFooterItem>
           </FeedFooter>
        </FeedBox>
    );
};