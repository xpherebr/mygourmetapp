import React from 'react';
import {Dimensions } from 'react-native';
import styled from 'styled-components/native';

const {width} = Dimensions.get('window');
const Container =  styled.SafeAreaView`
    margin-top: 30px;
`;
const List = styled.FlatList``;
const ListTitle = styled.Text`
    font-weight: bold;
    font-size:18px;
    margin-left:12px;
`;
const ListView = styled.TouchableOpacity`
    background-color:#fff;
    padding-top:10px;
    border-radius:10px;
`;
const ListViewImg = styled.Image``;
const ListViewName = styled.Text`
    margin-left:12px;
    font-size:16px;
    color: #b9b9b9;
`;

export default ({data}) => {
    const handlerChoiceIndication = (key) =>{
        alert(key);
    };

    return(
        <Container>
            <ListTitle>Deu match !</ListTitle>
            <List 
                data={data} 
                showsHorizontalScrollIndicator={false}
                snapToOffsets={[...Array(data.length)].map(
                    (x,i) => i * (width * 0.8 - 40) + (i - 1) * 40,
                )}
                keyExtractor={(item) => String(item.id)} 
                horizontal
                snapToAlignment={'Start'}
                scrollEventthrottle={16}
                decelerationRate="fast"
                renderItem={({item}) => 
                    <ListView activeOpacity={1} onPress={()=>handlerChoiceIndication(item.category)}>
                        <ListViewImg style={{
                            height: (width/2),
                            width: width * 0.8 - 20,
                            marginHorizontal:10,
                            borderRadius:5
                        }} source={{uri:item.uri}} resizeMode='contain'/>
                        <ListViewName>{item.nome}</ListViewName>
                    </ListView>

                }
            />
        </Container>
    );
}