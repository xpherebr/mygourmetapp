import React from 'react';
import styled from 'styled-components/native';

const TabArea =  styled.View`
    height: 50px;
    background-Color:#fff;
    flex-direction:row;
    border-top-color: #e5e5e5;
    border-top-width: 0.6px;
`;
const TabItem = styled.TouchableOpacity`
    flex:1;
    align-items:center;
    justify-content:center;
    margin-top: 5px;
`;
const TabItemImg = styled.Image`
    height:20px;
    width:20px;
`;
const TabItemTxt = styled.Text`
    color:#000;
    font-size: 12px;
`;
export default ({state,navigation}) => {
    const goTo = (screenName)=>{
        navigation.navigate(screenName);
    };

    return(
        <TabArea>
            <TabItem onPress={()=>goTo('Home')}>
                <TabItemImg style={{opacity: state.index===0?1:0.5}} source={require('../assets/lar-black.png')} resizeMode='contain'/>
                <TabItemTxt style={{opacity: state.index===0?1:0.5}} >Inicio</TabItemTxt>
            </TabItem>
            <TabItem onPress={()=>goTo('Search')}>
                <TabItemImg style={{opacity: state.index===1?1:0.5}} source={require('../assets/procurar-black.png')} resizeMode='contain'/>
                <TabItemTxt  style={{opacity: state.index===1?1:0.5}}>Buscar</TabItemTxt>
            </TabItem>
            <TabItem onPress={()=>{}}>
                <TabItemImg style={{opacity: state.index===1?1:0.5}} source={require('../assets/adicionar.png')} resizeMode='contain'/>
                <TabItemTxt  style={{opacity: state.index===1?1:0.5}}>Novo</TabItemTxt>
            </TabItem>
            <TabItem onPress={()=>goTo('Recipe')}>
                <TabItemImg style={{opacity: state.index===2?1:0.5}} source={require('../assets/receita-black.png')} resizeMode='contain'/>
                <TabItemTxt style={{opacity: state.index===2?1:0.5}}>Receitas</TabItemTxt>
            </TabItem>
            <TabItem onPress={()=>goTo('Profile')}>
                <TabItemImg style={{opacity: state.index===3?1:0.5}} source={require('../assets/perfil-black.png')} resizeMode='contain'/>
                <TabItemTxt  style={{opacity: state.index===3?1:0.5}}>Perfil</TabItemTxt>
            </TabItem>
        </TabArea>
    );
}