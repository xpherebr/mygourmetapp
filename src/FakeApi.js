export default {
    singIn: (email, password) => {
        let login = "teste@teste.com";
        let pass = "123456";
        let res = {
            ok: false,
        }

        if (email == login && pass == password) {
            res.ok = true;
            res.token = 'wçjvnawrvuibaruibvlçriuvbrçliub'
            res.name = 'Testador'
            return res;
        } else {
            return res;
        }
    },
    checkToken: (token) => {
        let res = {
            ok: false
        }

        if (token == 'wçjvnawrvuibaruibvlçriuvbrçliub') {
            res.ok = true;
            res.token = 'wçjvnawrvuibaruibvlçriuvbrçliub'
            res.name = 'Testador'
            return res;
        } else {
            return res;
        }
    },
    getIndications: () => {
        let res = {
            list:[
                {   
                    id:"1",
                    category:"1",
                    nome:'Massa',
                    uri: "https://ik.imagekit.io/wifire/blog/wp-content/uploads/2020/10/21134936/angulo-direto-300x200.jpg"
                },
                {
                    id:"2",
                    category:"2",
                    nome:'Integral',
                    uri: "https://img.itdg.com.br/tdg/images/blog/uploads/2018/12/Comida-chinesa-300x200.jpg"
                },
                {
                    id:"3",
                    category:"3",
                    nome:'Brasileira',
                    uri: "http://www.se.senac.br/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-20-at-08.15.12-300x200.jpeg"
                },
                {
                    id:"4",
                    category:"4",
                    nome:'Bolo',
                    uri: "https://casalmasah.com/wp-content/uploads/2021/04/Frutta-e-crema-comida-300x200.jpg.webp"
                },
                {
                    id:"5",
                    category:"5",
                    nome:'Mexicana',
                    uri: "https://www.amazonasemais.com.br/wp-content/uploads/2020/08/salta-comida-300x200.jpg"
                },
                {
                    id:"6",
                    category:"6",
                    nome:'Sobremesa',
                    uri: "https://blog.viajarfazbem.com/wp-content/webp-express/webp-images/uploads/2020/04/comidastipicasdobelemdoparabolodemacaxeira-300x200.jpg.webp"
                },
                {
                    id:"7",
                    category:"7",
                    nome:'Steak',
                    uri: "https://www.cidadeecultura.com/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2018/02/Sao-Paulo-Gastronomia-Brasileira-Restaurante-Emporio-Nordestino-web-300x200.jpg.webp"
                },
                {
                    id:"8",
                    category:"8",
                    nome:'Sucesso na Região',
                    uri: "https://uploads.emaisgoias.com.br/2021/04/7e45a7d6-comida-caipira-em-goiania-capa-300x200.jpg"
                },
                {
                    id:"9",
                    category:"9",
                    nome:'Frutos do Mar',
                    uri: "https://www.vero.com.br/wp-content/uploads/2021/10/Co%CC%81pia-de-CHP_24051-14-300x200.jpg"
                }
            ]
        }

        return res;
    },
    getRecipeForId:(id)=>{
    },
    getRecipes:()=>{
        const list = [
            {
                id:'1',
                category:'4',
                uri:'https://i.pinimg.com/originals/69/28/fb/6928fb3a551aab9484596e6dfed37036.png',
                description:'Explosão de Chocolate',
                chief:'Hélio Duarte',
                timeforpreppare:'40 a 50 Minutos',
                stars:'4.5',
                views:'450',
                origin:'Brasil',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'2',
                category:'1',
                uri:'https://i.pinimg.com/originals/2e/61/79/2e6179b8b067b7e5c4be70d8959d3c62.png',
                description:'Macarronada 3 Cores',
                chief:'Míqueias Ohcara',
                timeforpreppare:'40 a 50 Minutos',
                stars:'4',
                views:'600',
                origin:'Itália',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'3',
                category:'2',
                uri:'https://aprendercozinhar.com.br/wp-content/uploads/2020/02/PAO-DE-MINUTO-INTEGRAL-300x300.png',
                description:'Pão Integral de 1 Minuto',
                chief:'Míqueias Ohcara',
                timeforpreppare:'5 a 10 Minutos',
                stars:'3.8',
                views:'300',
                origin:'Itália',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'4',
                category:'3',
                uri:'https://www.padangastronomia.com.br/uploads/modules/itens/307619_foto_mini.png',
                description:'Pão Integral de 1 Minuto',
                chief:'Hélio Duarte',
                timeforpreppare:'20 a 40 Minutos',
                stars:'3.8',
                views:'1.5K',
                origin:'Brasil',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'5',
                category:'5',
                uri:'https://www.designi.com.br/images/preview/10060736-m.jpg',
                description:'Tacos',
                chief:'Isabela Gonzales',
                timeforpreppare:'20 a 40 Minutos',
                stars:'5.0',
                views:'3K',
                origin:'Mexico',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'6',
                category:'6',
                uri:'https://i.pinimg.com/originals/80/49/10/8049104471e3b733959abdbd3c13f0b3.png',
                description:'Pudim Branco',
                chief:'Isabela Gonzales',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'3K',
                origin:'Portugal',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'7',
                category:'7',
                uri:'https://www.lazeraoponto.com.br/wp-content/uploads/fly-images/940/costela-300x300-c.png',
                description:'Costela ao Forno',
                chief:'Hélio Duarte',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'2.6K',
                origin:'Portugal',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'8',
                category:'8',
                uri:'https://media.seumenu.com/blog/2021/09/comida-nordestina-300x300.png',
                description:'CuzCuz Recheado a Moda',
                chief:'Míqueias Ohcara',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'2.6K',
                origin:'Brasil',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'9',
                category:'9',
                uri:'https://i0.wp.com/pezinhoforadecasa.com/wp-content/uploads/2021/06/Design-sem-nome-20.png',
                description:'Camarão Toque dos Deuses',
                chief:'Míqueias Ohcara',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'2.6K',
                origin:'Brasil',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'10',
                category:'4',
                uri:'https://i.pinimg.com/originals/69/28/fb/6928fb3a551aab9484596e6dfed37036.png',
                description:'Explosão de Chocolate',
                chief:'Hélio Duarte',
                timeforpreppare:'40 a 50 Minutos',
                stars:'4.5',
                views:'450',
                origin:'Brasil',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'11',
                category:'1',
                uri:'https://i.pinimg.com/originals/2e/61/79/2e6179b8b067b7e5c4be70d8959d3c62.png',
                description:'Macarronada 3 Cores',
                chief:'Míqueias Ohcara',
                timeforpreppare:'40 a 50 Minutos',
                stars:'4',
                views:'600',
                origin:'Itália',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'12',
                category:'2',
                uri:'https://aprendercozinhar.com.br/wp-content/uploads/2020/02/PAO-DE-MINUTO-INTEGRAL-300x300.png',
                description:'Pão Integral de 1 Minuto',
                chief:'Míqueias Ohcara',
                timeforpreppare:'5 a 10 Minutos',
                stars:'3.8',
                views:'300',
                origin:'Itália',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'13',
                category:'3',
                uri:'https://www.padangastronomia.com.br/uploads/modules/itens/307619_foto_mini.png',
                description:'Pão Integral de 1 Minuto',
                chief:'Hélio Duarte',
                timeforpreppare:'20 a 40 Minutos',
                stars:'3.8',
                views:'1.5K',
                origin:'Brasil',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'14',
                category:'5',
                uri:'https://www.designi.com.br/images/preview/10060736-m.jpg',
                description:'Tacos',
                chief:'Isabela Gonzales',
                timeforpreppare:'20 a 40 Minutos',
                stars:'5.0',
                views:'3K',
                origin:'Mexico',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'15',
                category:'6',
                uri:'https://i.pinimg.com/originals/80/49/10/8049104471e3b733959abdbd3c13f0b3.png',
                description:'Pudim Branco',
                chief:'Isabela Gonzales',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'3K',
                origin:'Portugal',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'16',
                category:'7',
                uri:'https://www.lazeraoponto.com.br/wp-content/uploads/fly-images/940/costela-300x300-c.png',
                description:'Costela ao Forno',
                chief:'Hélio Duarte',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'2.6K',
                origin:'Portugal',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'17',
                category:'8',
                uri:'https://media.seumenu.com/blog/2021/09/comida-nordestina-300x300.png',
                description:'CuzCuz Recheado a Moda',
                chief:'Míqueias Ohcara',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'2.6K',
                origin:'Brasil',
                favorite:true,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },
            {
                id:'18',
                category:'9',
                uri:'https://i0.wp.com/pezinhoforadecasa.com/wp-content/uploads/2021/06/Design-sem-nome-20.png',
                description:'Camarão Toque dos Deuses',
                chief:'Míqueias Ohcara',
                timeforpreppare:'60 a 90 Minutos',
                stars:'5.0',
                views:'2.6K',
                origin:'Brasil',
                favorite:false,
                preparation:[
                    {
                        order:1,
                        step:'Em uma batedeira, bata o Leite MOÇA e o óleo por cerca de 5 minutos.',
                    },
                    {
                        order:2,
                        step:'Junte os ovos um a um, as raspas de laranja e o sal.',
                    },
                    {
                        order:3,
                        step:'Desligue a batedeira e acrescente aos poucos a fécula, a farinha de trigo e o fermento, peneirados juntos, alternando com o vinho.',
                    },
                    {
                        order:4,
                        step:'Distribua em duas fôrmas para bolo inglês pequenas (9 x 25 x 7 cm), untadas e enfarinhadas.',
                    },
                    {
                        order:5,
                        step:'Asse em forno médio (180ºC) preaquecido, por aproximadamente 45 minutos.',
                    },
                    {
                        order:6,
                        step:'Sirva frio.',
                    },
                ],
                ingredients:[
                    {
                        order:1,
                        description:'Leite Moça (lata ou caixinha)',
                        um:'Und',
                        portion:'395g',
                    },
                    {
                        order:2,
                        description:'Óleo',
                        um:'Xícara(chá)',
                        portion:'½',
                    },
                    {
                        order:3,
                        description:'Ovos',
                        um:'Und',
                        portion:'4',
                    },
                    {
                        order:4,
                        description:'Raspas da casca de laranja',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:5,
                        description:'Sal',
                        um:'Pitada',
                        portion:'1',
                    },
                    {
                        order:6,
                        description:'Fécula de batata',
                        um:'Xícara(chá)',
                        portion:'1 e ½',
                    },
                    {
                        order:7,
                        description:'Farinha de trigo',
                        um:'Xícara(chá)',
                        portion:'1',
                    },
                    {
                        order:8,
                        description:'Fermento químico em pó',
                        um:'Colher(sopa)',
                        portion:'1',
                    },
                    {
                        order:9,
                        description:'Vinho tinto seco',
                        um:'Xícara(chá)',
                        portion:'x½',
                    }, 
                ],
            },


        ]
        return list;

    },
    getUser:()=>{
        const user = {
            name:'Hélio Duarte',
            login:"teste@teste.com",
            password:"123456",
            age:'32',
            experience:'8',
            workplace:'R. Antônio Lima, 100 - Meireles, Fortaleza - CE, 60115-270',
            specialty:'Italian Food',
            avatar:"https://conteudo.imguol.com.br/c/noticias/45/2016/04/27/27abr2016---com-nuvens-de-farinha-se-espalhando-pelo-ar-esta-imagem-de-um-cozinheiro-trabalhando-a-massa-tem-segundo-o-critico-gastronomico-e-presidente-do-comite-de-jurados-jay-rayner-um-adoravel-1461766329138_300x300.jpg.webp",
        }

        return user;
    },
    getFeeds:()=>{
        const feeds =[
            {
                id:'1',
                avatar:"https://conteudo.imguol.com.br/c/noticias/45/2016/04/27/27abr2016---com-nuvens-de-farinha-se-espalhando-pelo-ar-esta-imagem-de-um-cozinheiro-trabalhando-a-massa-tem-segundo-o-critico-gastronomico-e-presidente-do-comite-de-jurados-jay-rayner-um-adoravel-1461766329138_300x300.jpg.webp",
                name:'Hélio Duarte',
                status:'Online',
                uri:'https://cdn.oimenu.com.br/base/178/254/5ac/fotografia_de_panqueca.jpg',
                issuedate:'1 d',
                favorite:true,
            },
            {
                id:'2',
                avatar:'https://admin.lucianapepino.com.br/wp-content/uploads/cirurgia-plastica-em-homens-1.jpg',
                name:'Miqueias Ohcara',
                status:'Offline',
                uri:'https://img.itdg.com.br/tdg/images/blog/uploads/2018/01/salada-grao-de-bico.jpg',
                issuedate:'5 d',
                favorite:false,
            },
            {
                id:'3',
                avatar:'https://i.pinimg.com/474x/12/ce/2f/12ce2fa9a427d72561d84c0b2887d5b2.jpg',
                name:'Adriana Mattos',
                status:'Online',
                uri:'https://p2.trrsf.com/image/fget/cf/648/0/images.terra.com/2018/09/17/torta-frigideira-presunto-queijo.jpg',
                issuedate:'1 Semana',
                favorite:false,
            },
            {
                id:'4',
                avatar:'https://beautyglobal.com.br/wp-content/uploads/2021/10/image-300x300.png',
                name:'Nicole Ferraz',
                status:'Offline',
                uri:'https://www.divinho.com.br/blog/wp-content/uploads/2020/10/vinho-californiano.jpg',
                issuedate:'1 Mês',
                favorite:true,
            },
        ]

        return feeds;
    },
};