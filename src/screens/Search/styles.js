import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView `
    flex: 1;
    background-color: #fff;
`;
export const SearchArea = styled.View`
    width:100%;
`;
export const SearchInput = styled.TextInput`
    height: 50px;
    background-color: #efefef;
    margin-top: 35px;
    margin-left: 15px;
    margin-right: 15px;
    border-radius: 5px;
    font-size: 19px;
    padding-left: 15px;
    padding-right: 15px;
    color: #6b6b6b;
`;
export const SearchStackArea = styled.View`
    flex: 1;
    margin-left: 10px;
    margin-right: 10px;
    background-color: #fff;
`;