import React,{ useState, useEffect }  from 'react';
import {StatusBar} from 'react-native';
import {Container,SearchInput,SearchArea,SearchStackArea} from './styles';

import ListVertical from '../../components/ListVertical';
import SearchStack from '../../stacks/SearchStack';
import FakeApi from '../../FakeApi';

export default ()=>{
    const recipes = FakeApi.getRecipes();
    const [searchText, setSearchText] = useState('');
    const [list, setList] = useState(recipes);

    useEffect(() => {
        if (searchText === '') {
          setList(recipes);
        } else {
          setList(
            recipes.filter(
              (item) =>
                item.description.toLowerCase().indexOf(searchText.toLowerCase()) > -1
            )
          );
        }
    }, [searchText]);

    return(
        <Container>
            <SearchArea>
                <SearchInput 
                    placeholder="Pesquisar..."
                    placeholderTextColor="#888"
                    value={searchText}
                    onChangeText={(t) => setSearchText(t)}
                />
            </SearchArea>
            <SearchStackArea>
              <SearchStack></SearchStack>
            </SearchStackArea>
            <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#fff" translucent = {true}/>
        </Container>
    );
}