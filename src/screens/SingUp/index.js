import React,{useState} from 'react';
import {useNavigation} from '@react-navigation/native'
import {StatusBar} from 'react-native';
import {
  Container,
  Logo,
  InputArea,
  CustomButton,
  CustomButtonText,
  SingMessageButton,
  SingMessageButtonText,
  SingMessageButtonTextBold 
} from './styles';

import SingInput from '../../components/SingInput';

export default () => {
  const navigation = useNavigation();  

  const [nameField, setNameField] = useState('');  
  const [emailField, setEmailField] = useState('');
  const [passWordField, setPassWordField] = useState('');

  const handleMessageButtonClick = ()=>{
    navigation.reset({
      routes:[{name:'SingIn'}]
    });
  };

  const handleSingClick = ()=>{

  }

  return(
    <Container>
      <Logo source={require('../../assets/logo.png')} />

      <InputArea>

      <SingInput 
          placeholder="Digite seu nome"
          value={nameField}
          onChangeText={t=>setNameField(t)}
        />

        <SingInput 
          placeholder="Digite seu e-mail"
          value={emailField}
          onChangeText={t=>setEmailField(t)}
        />

        <SingInput 
          placeholder="Digite sua senha"
          value={passWordField} 
          onChangeText={t=>setPassWordField(t)}
          password={true}
        />

        <CustomButton onPress={handleSingClick}>
          <CustomButtonText>CADASTRAR</CustomButtonText>
        </CustomButton>
      </InputArea>

      <SingMessageButton onPress={handleMessageButtonClick}>
        <SingMessageButtonText>Já possui uma conta?</SingMessageButtonText>
        <SingMessageButtonTextBold>Faça login</SingMessageButtonTextBold>
      </SingMessageButton>
      <StatusBar barStyle = "default" hidden = {false} backgroundColor = "#D2691E" translucent = {true}/>
    </Container>
  );
}