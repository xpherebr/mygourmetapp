import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.ScrollView `
    flex: 1;
    background-color: #fff;
`;
export const HeaderProfile = styled.View`
    flex-direction:row;
    height: 100px;
    width: 100%;
    background-color: #E9A46A;
    border-bottom-right-radius: 50px;
`;
export const HeaderAvatarImg = styled.View`
    height: 120px;
    width: 120px;
    background-color: #fff;
    margin-top: 40px;
    margin-left:10px;
    border-color: #FFF;
    border-width: 3px;
    border-radius: 60px;
`;
export const AvatarImg = styled.Image`
    height: 100%;
    width: 100%;
    border-radius: 60px;
`;
export const HeaderContent = styled.View`
    flex: 1;
    flex-direction: row;
    height: 120px;
    margin-right:20px;
    justify-content: space-around;
    
`;
export const HeaderContentIndicator = styled.View` 
    width: 60px;
    height: 100%;
    align-items:center;
    justify-content:center;
    margin-top: 10px;
`;
export const HeaderContentIndicatorTextBold = styled.Text`
    font-size:18px;
    font-weight:bold;
    color: #444444;
`;
export const HeaderContentIndicatorText = styled.Text`
    font-size:12px;
    color: #6b6b6b;
    margin-top: -8px;
`;
export const HeaderContentName = styled.Text`
    font-size:24px;
    font-weight:bold;
    color: #444444;
    margin-left: 140px;
`;
export const BodyContainer = styled.View`
    margin-top:60px;
    justify-content:center;
    margin-bottom:60px;
`;
export const FooterContainer = styled.View``;
export const LinkContent = styled.TouchableOpacity`
    flex-direction:row;
    align-items:center;
    margin-left:10px;
    margin-right:10px;
    height: 50px;
`;
export const LinkContentBorder = styled.View`
    border-bottom-width: 1px;
    border-bottom-color: #6b6b6b;
    margin-left:20px;
    margin-right:20px;
    opacity:0.2;
`;
export const LinkContentIcon = styled.Image`
    height: 20px;
    width: 20px;
`;
export const LinkContentText = styled.Text`
    font-size:18px;
    color: #000;
    margin-left: 10px;
`;
export const LinkContentNext = styled.View`
    flex: 1;
    align-items:flex-end;
    justify-content:flex-end;
`;
export const LinkContentNextIcon = styled.Image`
    height: 12px;
    width: 12px;
    margin-top:4px;
    margin-left:10px;
    opacity: 0.5;
`;