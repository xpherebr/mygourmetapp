import React from 'react';
import {StatusBar} from 'react-native';
import {
    Container,
    HeaderProfile,
    HeaderAvatarImg,
    AvatarImg,
    HeaderContentIndicator,
    HeaderContent,
    HeaderContentIndicatorTextBold,
    HeaderContentIndicatorText,
    HeaderContentName,
    BodyContainer,
    FooterContainer,LinkContent,LinkContentIcon,LinkContentText,LinkContentNext,
    LinkContentBorder,LinkContentNextIcon,
} from './styles';

import FakeApi from '../../FakeApi'

export default ()=>{

    const user = FakeApi.getUser();

    return(
        <Container>
            <HeaderProfile>
                <HeaderAvatarImg>
                    <AvatarImg  source={{uri:user.avatar}}/>
                </HeaderAvatarImg>
                <HeaderContent>
                    <HeaderContentIndicator>
                            <HeaderContentIndicatorTextBold>50</HeaderContentIndicatorTextBold>
                            <HeaderContentIndicatorText>receitas</HeaderContentIndicatorText>
                    </HeaderContentIndicator>

                    <HeaderContentIndicator>
                        <HeaderContentIndicatorTextBold>15</HeaderContentIndicatorTextBold>
                        <HeaderContentIndicatorText>seguindo</HeaderContentIndicatorText>
                    </HeaderContentIndicator>

                    <HeaderContentIndicator>
                        <HeaderContentIndicatorTextBold>50</HeaderContentIndicatorTextBold>
                        <HeaderContentIndicatorText>seguido</HeaderContentIndicatorText>
                    </HeaderContentIndicator>

                    <HeaderContentIndicator>
                        <HeaderContentIndicatorTextBold>4.5</HeaderContentIndicatorTextBold>
                        <HeaderContentIndicatorText>estrelas</HeaderContentIndicatorText>
                    </HeaderContentIndicator>
                </HeaderContent>
            </HeaderProfile>
            <HeaderContentName>{user.name}</HeaderContentName>
            <BodyContainer>
                <LinkContent>
                    <LinkContentIcon source={require('../../assets/notificacao.png')} />
                    <LinkContentText>Notificações</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

                <LinkContent>
                    <LinkContentIcon source={require('../../assets/coracao.png')} />
                    <LinkContentText>Favoritos</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

                <LinkContent>
                    <LinkContentIcon source={require('../../assets/view.png')} />
                    <LinkContentText>Views</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

                <LinkContent>
                    <LinkContentIcon source={require('../../assets/dolar.png')} />
                    <LinkContentText>Publicidade</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

                <LinkContent>
                    <LinkContentIcon source={require('../../assets/comente.png')} />
                    <LinkContentText>Chat</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

                <LinkContent>
                    <LinkContentIcon source={require('../../assets/marcador.png')} />
                    <LinkContentText>Endereço</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

                <LinkContent>
                    <LinkContentIcon source={require('../../assets/perfil-black.png')} />
                    <LinkContentText>Meus Dados</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

            </BodyContainer>
            <FooterContainer>
                <LinkContent>
                    <LinkContentIcon source={require('../../assets/configuracao.png')} />
                    <LinkContentText>Configurações</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />
                
                <LinkContent>
                    <LinkContentIcon source={require('../../assets/seguranca.png')} />
                    <LinkContentText>Segurança</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>
                <LinkContentBorder />

                <LinkContent>
                    <LinkContentIcon source={require('../../assets/ajuda.png')} />
                    <LinkContentText>Ajuda</LinkContentText>
                    <LinkContentNext>
                        <LinkContentNextIcon source={require('../../assets/next.png')} />
                    </LinkContentNext>
                </LinkContent>

            </FooterContainer>
            <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#fff" translucent = {true}/>
        </Container>
    );
}