import React from "react";
import { StatusBar } from "react-native";
import { Container, ListFeed } from "./styles";

import FeedComponent from "../../components/FeedComponent";

import FakeApi from "../../FakeApi";

export default () => {
  const data = FakeApi.getFeeds();

  return (
    <Container>
      <ListFeed
        data={data}
        vartical
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => <FeedComponent data={item} />}
      />
      <StatusBar
        barStyle="dark-content"
        hidden={false}
        backgroundColor="#fff"
        translucent={true}
      />
    </Container>
  );
};
