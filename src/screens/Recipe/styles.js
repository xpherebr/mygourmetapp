import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView `
    flex:1;
    background-color:#fff;
`;
export const Title = styled.Text`
    font-weight: bold;
    font-size:18px;
    margin-left:12px;
    margin-top: 10px;
`;