import React from 'react';
import {StatusBar} from 'react-native';
import {Container,Title} from './styles';

import ListHorizon from '../../components/ListHorizon';
import ListVertical from '../../components/ListVertical';
import FakeApi from '../../FakeApi';

export default ()=>{

    const aIndications = FakeApi.getIndications();
    const recipes = FakeApi.getRecipes();

    return(
        <Container>
            <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#fff" translucent = {true}/>
            <ListHorizon data={aIndications.list}/>
            <Title>Novidades :)</Title>
            <ListVertical data={recipes}/>
        </Container>
    );
}