import React,{useEffect,useContext} from 'react';
import {StatusBar} from 'react-native';
import {Container,LoadingIcon,Logo} from './styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

import FakeApi from '../../FakeApi';

import {UserContext} from '../../contexts/UserContext'

export default () => {
  const {dispatch: userDispatch} = useContext(UserContext);

  const navigation = useNavigation();

  //AsyncStorage.setItem('token','')

  useEffect(()=>{
    const checkToken = async () =>{
      //AsyncStorage.setItem('token','')
      const token = await AsyncStorage.getItem('token');
      
      if(token !== null){
        let json = FakeApi.checkToken(token);

        if(json.ok){
          AsyncStorage.setItem('token',json.token)

          userDispatch({
            type:'setName',
            payload: {
              name: json.name
            }
          });

          navigation.reset({
            routes:[{name:'MainTab'}]
          });

        }else{
          navigation.reset({
            routes:[{name:'SingIn'}]
          });
        }

       
      }else{
        navigation.reset({
          routes:[{name:'SingIn'}]
        });
      }
    }
    checkToken();
  },[]);

  return(
    <Container>
      <StatusBar barStyle = "default" hidden = {false} backgroundColor = "#D2691E" translucent = {true}/>
      <Logo source={require('../../assets/logo.png')} />
      <LoadingIcon size="large" color="#d3d3d3" />
    </Container>
  );
}