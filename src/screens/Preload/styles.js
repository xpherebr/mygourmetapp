import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView `
    flex: 1;
    backgroundColor:#E9A46A
    align-items: center;
    justify-content:center;
`;
export const Logo = styled.Image`
    width: 200px;
    height: 200px;
`;
export const LoadingIcon = styled.ActivityIndicator`
    margin-top:50px;
`;
