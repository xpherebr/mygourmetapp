import React,{useState, useContext} from 'react';
import {useNavigation} from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import {StatusBar} from 'react-native';

import {UserContext} from '../../contexts/UserContext'

import {
  Container,
  Logo,
  InputArea,
  CustomButton,
  CustomButtonText,
  SingMessageButton,
  SingMessageButtonText,
  SingMessageButtonTextBold 
} from './styles';

import SingInput from '../../components/SingInput';

import FakeApi from '../../FakeApi';

export default () => {
  const {dispatch: userDispatch} = useContext(UserContext);

  const navigation = useNavigation();  
  const [emailField, setEmailField] = useState('');
  const [passWordField, setPassWordField] = useState('');

  const handleMessageButtonClick = ()=>{
    navigation.reset({
      routes:[{name:'SingUp'}]
    });
  };

  const handleSingClick = ()=>{
      if(emailField != '' && passWordField != ''){

        let json = FakeApi.singIn(emailField,passWordField);

        if(json.ok){
          AsyncStorage.setItem('token',json.token)

          userDispatch({
            type:'setName',
            payload: {
              name: json.name
            }
          });

          navigation.reset({
            routes:[{name:'MainTab'}]
          });

        }else{
          alert('E-mail e/ou senha errados!');
        }

      }else{
        alert('Preencha os campos!');
      }
  }

  return(
    <Container>
      <Logo source={require('../../assets/logo.png')} />

      <InputArea>

        <SingInput 
          placeholder="Digite seu e-mail"
          value={emailField}
          onChangeText={t=>setEmailField(t)}
        />

        <SingInput 
          placeholder="Digite sua senha"
          value={passWordField} 
          onChangeText={t=>setPassWordField(t)}
          password={true}
        />

        <CustomButton onPress={handleSingClick}>
          <CustomButtonText>LOGIN</CustomButtonText>
        </CustomButton>
      </InputArea>

      <SingMessageButton onPress={handleMessageButtonClick}>
        <SingMessageButtonText>Ainda não possui uma conta?</SingMessageButtonText>
        <SingMessageButtonTextBold>Cadastre-se</SingMessageButtonTextBold>
      </SingMessageButton>
      <StatusBar barStyle = "default" hidden = {false} backgroundColor = "#D2691E" translucent = {true}/>
    </Container>
  );
}