import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView `
    flex: 1;
    background-Color:#E9A46A
    align-items: center;
    justify-content:center;
`;
export const Logo = styled.Image`
    width: 200px;
    height: 200px;
`;
export const InputArea = styled.View`
    width: 100% ;
    padding:40px;
`;
export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    background-Color:#D2691E;
    border-radius:30px;
    align-items: center;
    justify-content:center;
`;
export const CustomButtonText = styled.Text`
    font-size:18px;
    color:#fff;
`;
export const SingMessageButton = styled.TouchableOpacity`
    flex-direction:row;
    justify-content:center;
    margin-top:50px;
    margin-bottom:20px;
`;
export const SingMessageButtonText = styled.Text`
    font-size:16px;
    color:	#FFDEAD;
`;
export const SingMessageButtonTextBold = styled.Text`
    font-size:16px;
    color:	#FFDEAD;
    font-weight: bold;
    margin-left: 5px;
`;