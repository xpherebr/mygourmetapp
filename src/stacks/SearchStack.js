import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Persons from '../screens/SearchPerson';
import Locals from '../screens/SearchLocals';
import Recipes from '../screens/SearchRecipes';

const Tab = createMaterialTopTabNavigator();

export default () => (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: '#E9A46A',
        tabBarInactiveTintColor: '#444444',
        tabBarLabelStyle: { fontSize: 12 },
        tabBarIndicatorStyle: {
          backgroundColor: "#E9A46A",
        },
      }}
    >
      <Tab.Screen name="Pessoas" component={Persons} />
      <Tab.Screen name="Locais" component={Locals} />
      <Tab.Screen name="Receitas" component={Recipes} />
    </Tab.Navigator>
);