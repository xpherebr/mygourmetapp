import React from 'react'
import {createNativeStackNavigator} from '@react-navigation/native-stack'

import Preload from '../screens/Preload'
import SingIn from '../screens/SingIn'
import SingUp from '../screens/SingUp'
import MainTab from '../stacks/MainTab'

const Stack = createNativeStackNavigator();

export default () => (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Preload"  component={Preload} />
        <Stack.Screen name="SingIn" component={SingIn} />
        <Stack.Screen name="SingUp" component={SingUp} />
        <Stack.Screen name="MainTab" component={MainTab} />
    </Stack.Navigator>
);