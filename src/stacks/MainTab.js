import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import CustonTabBar from '../components/CustomTabBar'

import Home from '../screens/Home';
import Search from '../screens/Search';
import Recipe from '../screens/Recipe';
import Profile from '../screens/Profile';

const Tab = createBottomTabNavigator();

export default () => (
    <Tab.Navigator screenOptions={{ headerShown: false }}  tabBar={props=><CustonTabBar {...props}/>}>
        <Tab.Screen name="Home"  component={Home} />
        <Tab.Screen name="Search" component={Search} />
        <Tab.Screen name="Recipe" component={Recipe} />
        <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
);