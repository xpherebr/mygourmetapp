export const initialState = {
    name:'',
    avatar: '',
    favorities: [],
}

export const UserReducer = (state,action) => {
    switch(action.type){
        case 'setAvatar': 
            return{...state,avatar:action.payload.avatar};
        break;
        case 'setName': 
            return{...state,name:action.payload.name};
        break;
        default:
        return state;
    }
}

